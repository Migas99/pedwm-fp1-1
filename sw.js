var cacheName = 'cache-v1';

var staticAssets = [
  './',
  './index.html',
  './css/main.css',
  './js/main.js',
  './img/header.png',
  './img/icons/icon-192x192.png',
  './img/icons/icon-512x512.png'
];


async function cacheFirst(req) {
  const cache = await caches.open(cacheName);
  const cachedResponse = await cache.match(req);
  return cachedResponse || networkFirst(req);
}

async function networkFirst(req) {
  const cache = await caches.open(cacheName);
  try { 
    const fresh = await fetch(req);
    cache.put(req, fresh.clone());
    return fresh;
  } catch (e) { 
    const cachedResponse = await cache.match(req);
    return cachedResponse;
  }
}


/* Start the service worker and cache all of the app's content */
self.addEventListener('install', async event => {
  const cache = await caches.open(cacheName); 
  try{
    await cache.addAll(staticAssets);
  } catch (e){
      console.log(e);
  }
});

/* Serve cached content when offline */
self.addEventListener('fetch', event => {
  const req = event.request;
  event.respondWith(networkFirst(req));
});